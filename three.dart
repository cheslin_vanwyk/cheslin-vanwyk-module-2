void main(List<String> args) {
  var wOne = App();
  wOne.name = "Ethereum";
  wOne.category = "Blockchain";
  wOne.developer = "Vitalik Buterin";
  wOne.date = 2016;

  wOne.pWinner();
  wOne.cLetter();
}

class App {
  String? name;
  String? category;
  String? developer;
  int? date;

  void pWinner() {
    print(name);
    print(category);
    print(developer);
    print(date);
  }

  void cLetter() {
    print(this.name?.toUpperCase());
  }
}
