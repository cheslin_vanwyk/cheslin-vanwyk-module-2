void main(List<String> args) {
  var winners = <String>[
    'FNB Banking',
    'Bookly',
    'Live-Inspect',
    'CPUT-mobile',
    'Domestly',
    'OrderIn',
    'Cowa-bunga',
    'Digger',
    'Checkers Sixty',
    'Ambani Afrika'
  ];
  //a)
  var clone = winners.toList();
  winners.sort();
  print(winners);

  //b)
  var w2017 = clone[2017 - 2012]; // because 2012 is positioned at index 0.
  var w2018 = clone[2018 - 2012];
  print(w2017 + '\n' + w2018);

  //c)
  print(winners.length);
}
